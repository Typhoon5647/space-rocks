{
    "id": "3f3a1797-6f21-4983-94fe-540b0b3a85ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "357798a9-86ea-4316-84f1-8647e5d712e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f3a1797-6f21-4983-94fe-540b0b3a85ca"
        },
        {
            "id": "0635b03d-766f-4cf1-aecc-ea3d31263e15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b9f17eaa-7cd8-4150-b465-a4d41223c4b1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3f3a1797-6f21-4983-94fe-540b0b3a85ca"
        },
        {
            "id": "471efed5-6d2e-42d7-805c-c2a9e11735e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "3f3a1797-6f21-4983-94fe-540b0b3a85ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f1ac8951-d0b2-49cc-95b1-52ee5816c908",
    "visible": true
}