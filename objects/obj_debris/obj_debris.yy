{
    "id": "07242747-e5c3-4f2f-b3c9-689fb6214d08",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_debris",
    "eventList": [
        {
            "id": "9820c259-e737-4645-8dc1-1ce96a589915",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "07242747-e5c3-4f2f-b3c9-689fb6214d08"
        },
        {
            "id": "d58272ca-8cd5-441e-964a-4db15403bb30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "07242747-e5c3-4f2f-b3c9-689fb6214d08"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "caa6ba29-1992-4eed-916c-09ac0d26cdac",
    "visible": true
}