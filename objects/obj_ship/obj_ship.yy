{
    "id": "5ef20290-8b66-4c38-ac6e-00b16bf78133",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "cc8e285d-7a88-4ae7-ad44-59231d725a80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ef20290-8b66-4c38-ac6e-00b16bf78133"
        },
        {
            "id": "66fc0c29-bb32-41d1-b34d-8dd5294bcacf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b9f17eaa-7cd8-4150-b465-a4d41223c4b1",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5ef20290-8b66-4c38-ac6e-00b16bf78133"
        },
        {
            "id": "8033afe6-f057-46aa-8914-5500613c2e14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "5ef20290-8b66-4c38-ac6e-00b16bf78133"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b2be37d2-7fe2-41f1-b512-4798ed04c826",
    "visible": true
}