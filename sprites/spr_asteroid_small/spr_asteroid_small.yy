{
    "id": "8c0533ab-4d48-4827-a220-a966257eae09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7bfda3e-f69b-4f2d-8233-08f1644e2337",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c0533ab-4d48-4827-a220-a966257eae09",
            "compositeImage": {
                "id": "45ba6513-b2e1-42b4-ae35-1e88e4771d67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7bfda3e-f69b-4f2d-8233-08f1644e2337",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f15563ef-cd08-4c4d-9165-ca20a809a605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7bfda3e-f69b-4f2d-8233-08f1644e2337",
                    "LayerId": "525cf56b-97e2-40e7-9299-d52e524f4d13"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "525cf56b-97e2-40e7-9299-d52e524f4d13",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c0533ab-4d48-4827-a220-a966257eae09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}