{
    "id": "120304ee-dbea-4eaf-88eb-97a672cd84ba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58a243ad-4852-41f4-a275-809d5139e6c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "120304ee-dbea-4eaf-88eb-97a672cd84ba",
            "compositeImage": {
                "id": "ff53bc64-c69c-4e92-b544-8caa7149fc39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58a243ad-4852-41f4-a275-809d5139e6c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "615a6dc4-d050-4f2a-adb7-690ccc16dbaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58a243ad-4852-41f4-a275-809d5139e6c8",
                    "LayerId": "5f05b22d-12f3-44dd-b837-66995b42a839"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "5f05b22d-12f3-44dd-b837-66995b42a839",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "120304ee-dbea-4eaf-88eb-97a672cd84ba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}