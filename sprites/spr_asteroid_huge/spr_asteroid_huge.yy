{
    "id": "419ca9b4-1671-4210-b960-368b2d9bb1a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 57,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c3a733f-6e42-44b1-8d30-938e19d1c4c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "419ca9b4-1671-4210-b960-368b2d9bb1a1",
            "compositeImage": {
                "id": "a7a5fa30-7fc4-4962-b607-f3951cf31a72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c3a733f-6e42-44b1-8d30-938e19d1c4c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca9c828e-38d7-4f67-bb23-ef4265a95397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c3a733f-6e42-44b1-8d30-938e19d1c4c7",
                    "LayerId": "a36eb24c-4db6-42a7-ab33-0d218c1558f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a36eb24c-4db6-42a7-ab33-0d218c1558f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "419ca9b4-1671-4210-b960-368b2d9bb1a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}