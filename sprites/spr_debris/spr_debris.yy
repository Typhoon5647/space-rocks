{
    "id": "caa6ba29-1992-4eed-916c-09ac0d26cdac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d4905113-a169-409e-9557-dbe236a7c11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "caa6ba29-1992-4eed-916c-09ac0d26cdac",
            "compositeImage": {
                "id": "8c5a6ba5-e0d4-4561-ac37-f5269b562bf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4905113-a169-409e-9557-dbe236a7c11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf5cbb0d-00d8-4474-a539-0c01eecb955f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4905113-a169-409e-9557-dbe236a7c11d",
                    "LayerId": "db74cb6d-4eec-42da-ac84-46da3795b229"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "db74cb6d-4eec-42da-ac84-46da3795b229",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "caa6ba29-1992-4eed-916c-09ac0d26cdac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}