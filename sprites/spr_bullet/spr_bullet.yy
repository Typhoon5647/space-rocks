{
    "id": "f1ac8951-d0b2-49cc-95b1-52ee5816c908",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f14075a-247f-42a2-8b33-71bb9349ee5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1ac8951-d0b2-49cc-95b1-52ee5816c908",
            "compositeImage": {
                "id": "f5ce50e3-f20c-49e4-abdd-c28a756818d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f14075a-247f-42a2-8b33-71bb9349ee5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61040cf0-28f3-435e-973f-617b2fecbcb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f14075a-247f-42a2-8b33-71bb9349ee5e",
                    "LayerId": "c77de1f0-99df-45d0-8436-230cb83a078e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "c77de1f0-99df-45d0-8436-230cb83a078e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1ac8951-d0b2-49cc-95b1-52ee5816c908",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}