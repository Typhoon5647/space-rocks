{
    "id": "b2be37d2-7fe2-41f1-b512-4798ed04c826",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 13,
    "bbox_right": 60,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3cc4aa04-34ca-468d-b909-88d13d31198e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2be37d2-7fe2-41f1-b512-4798ed04c826",
            "compositeImage": {
                "id": "c7a0a2e0-bdfd-461c-86a0-8996132d5b84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc4aa04-34ca-468d-b909-88d13d31198e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a790ba2b-7278-4981-b932-0198a928d3a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc4aa04-34ca-468d-b909-88d13d31198e",
                    "LayerId": "ca14b1d8-46b0-442a-82b0-b10848c94262"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ca14b1d8-46b0-442a-82b0-b10848c94262",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2be37d2-7fe2-41f1-b512-4798ed04c826",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}