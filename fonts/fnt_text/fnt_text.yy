{
    "id": "b3758026-a12d-47bc-926e-ace381283f3d",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_text",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Lucida Console",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "26df7c68-7902-4401-845e-c7c111f7b55d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e7d641e1-5a8a-41d6-953e-b9686c0a43a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 192,
                "y": 40
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "67e4b775-4111-43ec-ac17-8627f1374fc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 183,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6cfdd258-dd9f-4e50-9433-2cad78834c50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 171,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "0851acac-a56b-4f2a-8d54-d512906f7fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 161,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0d940dd9-94c5-4c58-9ef3-deb800ba0f03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7599e727-8a17-48da-9dbe-14a41d88af91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8154ef47-6f79-440e-ad92-321f401135f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 132,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "98e5f7e5-dd95-4f89-8ff4-9d82adb24a96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 123,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ad270edd-d548-45d4-8ad3-62642d744e7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 115,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2f3b14a8-0501-4156-83fa-411897752e67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 197,
                "y": 40
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "baee5946-45f7-42bc-a9ba-f9c331019e5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 104,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "982aa514-c5ab-47c6-bbbc-ce510618ebeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 90,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ed8e19fa-8b82-4b51-a72d-5997acbe947f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 81,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8aee0309-6565-432d-9910-0d02a8683543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 76,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "62cf5a24-ad53-417d-a00f-8368d8287658",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 64,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "12358c80-3b28-437d-98b3-56b39d396fd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 53,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0483ba8a-fe22-49ce-8c13-0c991dae5549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 42,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "98646a7d-56c8-40b9-9ac2-4c69b8d0ff39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 33,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8ed8e935-9d33-45a5-b2a5-bac99c5942dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 23,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ee3c5111-62a0-45ff-9bcf-3d174fd78faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 12,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b6439a95-359b-4535-b674-e546243762c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 95,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "3408b9e9-ca67-472b-8727-b1212cb869a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 207,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "731cf1f5-ec38-42da-b3d7-dabfafc73a78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 217,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6f37119a-781e-4e79-bc54-d945ecf6f74c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 227,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d926167e-0090-46c1-8f07-24953648344c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 194,
                "y": 59
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "da688de2-f087-4290-bfc1-7c5bdd3a7be8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 189,
                "y": 59
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "eb0a79e4-f161-434a-b78d-c56164af5505",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 3,
                "x": 184,
                "y": 59
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5fdbb9e4-2b00-4df2-9527-60929def00f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 173,
                "y": 59
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0f7b7f8c-ed79-4254-b014-0afbf78303d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 162,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6b24c602-5696-4fb1-a905-55d524c19015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 151,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "265f199f-4297-4998-8200-a99d608e4351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 141,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b32f1e7d-1e24-4c3b-9bca-f97015df4899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b5590b98-1ac4-4e56-af68-5ec5303409a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 117,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "52125833-0dc9-4a9a-b5d0-1cc652272e9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 107,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fed60715-4750-475c-bd31-3065993e596b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 95,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fa8d8574-fe17-4d02-9a3f-d853b342f063",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 84,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "a4478fc3-12a1-48c0-9275-b85d458f7562",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 74,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9e46e324-a103-4a24-9350-90a70357c6bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 63,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "879a2c1c-3edc-4bdd-928d-dabfaea99147",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 52,
                "y": 59
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fed950ae-c5f6-47e8-8dd2-3fed9cb4a2c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 59
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7cc779b7-eb40-4bb8-96ac-a3951277dc89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f067e606-725f-4db7-a04c-c4f9c072c6c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 23,
                "y": 59
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "1bec3887-9517-4398-9c33-f11aa7cbe4c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 12,
                "y": 59
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9844b156-0546-485f-9dfe-630449b35175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c32b2804-499e-40b9-92dc-5a70607c4fb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 237,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "7af102bb-ebcd-4f5d-9ab2-089f605b31fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b7d0d9b2-c04a-43d7-8ae6-3619604380a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 241,
                "y": 21
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "15a17053-701d-4efa-82bd-d5247baef9e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8cf61995-3b55-4f5f-8654-abe87ac290fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a8473a70-a68f-405f-a591-a0846b4ad125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "18e92fa3-a1f2-4192-a746-5a6bd45b2d6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dcc7406a-5b24-49d4-af16-efef24d44fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "90c352e0-b37b-4b5a-b82a-159978d246d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "c1dcd161-64ee-4c34-99bb-07a7ca64eb34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a43d45bb-00c8-42b2-bc11-0961a355381e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "44a6a479-ccd5-4936-8d2e-5f2ce5b688cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5536d55d-5e10-40e2-be7d-7f243e795f74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "9c5e8885-60a0-440c-8c1a-98f347c962fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "8d6300b6-cbc7-49c1-a864-09e88cd1e439",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 3,
                "shift": 10,
                "w": 6,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "98226ffa-abc4-4a6b-915f-039ef3e2a8d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "269b0da5-7605-483d-8054-e4a96e2dc134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fd2bcd0a-9909-4e23-b1e9-c2301a330051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ccbd58b4-8caf-43f9-b2a6-98cda0db5d60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "fb706aad-0937-42da-8032-8f20f8ce4200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 5,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b82a4c08-bd28-435d-82d8-0e26912a3e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b3673102-dbd5-4989-843c-96d5fb8a7484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "6a21fb95-76ba-490c-adcb-c6218444ec2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "25df0ef3-53f1-4fa3-b2f5-06ab6d63c6e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "38229e3d-7b53-440c-974a-b3ac78248d9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "5c2d441c-a042-4b3a-a7f1-edcb0d731e5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0df63d33-66bd-4215-a536-98ff59cb10b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "77bec17c-ab3c-423d-80dc-a7030ade96ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 111,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "98bae038-415f-45ce-848a-e070dde82535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 13,
                "y": 21
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "ee421039-3143-4c30-b5f7-3ea8e1ef027f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 212,
                "y": 21
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ffb80582-c90b-4c58-9d19-39c656e85332",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 201,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "07470302-ebf4-43a7-b991-99eb94d134b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 6,
                "x": 193,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a43bdbf9-1805-447f-80a8-187a65148ca9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 182,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3119dfb0-dc65-4ed9-affd-2094a16af917",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 172,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3b555e10-2733-4e0f-b03f-04bcc633ba3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 161,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "365c30ea-88fe-463e-80c5-bdfbf10ad14f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 151,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "cadb1b53-9db0-4ba3-8574-943bd8db6624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 140,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7381b4f7-98d9-4b4d-a1c4-a8d52ad2eb2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 2,
                "shift": 10,
                "w": 7,
                "x": 131,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5e3b9e92-8e89-4cdc-b20d-bc08cee4f753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 220,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "0acd660a-23d3-4ec0-b44d-f52ab4e07bdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 121,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9f80b75f-b890-448b-b2e5-d3349f7baa81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 101,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "63bb4e6c-2f47-4a27-bd6e-1f506790d53a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 89,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ac54a85f-b707-47e7-badd-0368ba7fdc00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 77,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "95e80063-699f-48f7-9406-94bf88b09fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 65,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "88ef86b3-31a7-4935-90a7-c1ae54a9268d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 53,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "978eec8b-d0ae-4117-877b-d9c70a935e7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 43,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "b32cef16-d0ea-4edf-8851-35bb8fc9a387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 34,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "ec4c7311-5dce-48f8-a297-4323ee9a400d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 4,
                "shift": 10,
                "w": 2,
                "x": 30,
                "y": 21
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0ebe891b-2707-40be-883f-31a82be38d69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 21,
                "y": 21
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8ff703c7-b676-4396-9cd9-498663d7f940",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 204,
                "y": 59
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "bd518ea1-d5a8-4bc6-9b56-74e4b5524811",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 215,
                "y": 59
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000d\\u000a0123456789 .,<>\"'&!?\\u000d\\u000athe quick brown fox jumps over the lazy dog\\u000d\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000d\\u000a\\u000d\\u000aDefault Character(9647) ",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}